import spacy
#nlp = spacy.load("de")
from spacy.lang.de import German
from collections import Counter
import de_core_news_sm
nlp = de_core_news_sm.load()

explain_file = open("explain.txt", "w")
token_count_file = open("token_count.txt", "w")
count_file = open("count.txt", "w")
lemma_file = open("lemma.txt", "w")
file = open("sentences.txt", "r")
for text in file:
  doc = nlp(text) 
  tok_exp = nlp.tokenizer.explain(text)
  explain_file.write(str(tok_exp)+"\n")
  lemmas = [token.lemma_ for token in doc]
  lemma_file.write(" ".join(lemmas))
  count = Counter(i[0] for i in tok_exp)
  token_count = count['TOKEN']
#   for i in tok_exp:
#       if i[0] == "TOKEN":
#         count = count + 1
  count_file.write(str(count)+"\n")  
  token_count_file.write(str(token_count)+"\n") 
token_count_file.close()
explain_file.close()
#   assert [t.text for t in doc if not t.is_space] == [t[1] for t in tok_exp]
#   for t in tok_exp:
#       print(t[1], "\t", t[0])
